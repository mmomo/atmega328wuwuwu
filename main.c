#ifndef F_CPU
#define F_CPU 16000000UL
#endif

#include <avr/io.h>
#include <util/delay.h>
#include <string.h>

#define setBit(port, bit) port |= (1 << bit)
#define clearBit(port, bit) port &= ~(1 << bit)

typedef uint8_t byte;

//#define PORT      PORTB
//#define DDR       DDRB

#define PORT      PORTC // lcd port
#define DDR       DDRC

#define LCD_E     5 // Px5 for LCD E
#define LCD_RS    4 // Px4 for LCD_RS
#define DAT7      3 // Px3 for data7
#define DAT6      2 // Px2 for data6
#define DAT5      1 // Px1 for data5
#define DAT4      0 // Px0 for data4

/* Misc routines */
void setupPorts (void);
void msDelay (int delay);

/* LCD routines */
#define CLEARDISPLAY 0x01
#define SETCURSOR    0x80

void pulseEnableLine (void);
void sendNibble (byte data);
void sendByte (byte data);
void LCD_Cmd (byte cmd);
void LCD_Char (byte ch);
void LCD_Init (void);
void LCD_Clear (void);
void LCD_Home (void);
void LCD_Goto (byte x, byte y);
void LCD_Line (byte row);
void LCD_Message (const char *text);

/*************************************************/
int main (void)
{
  setupPorts();
  LCD_Init();
  
  while (1) {
    
    LCD_Message("Pato Momo presenta:");
    msDelay(1000);
    LCD_Clear();
    
    LCD_Goto(2, 1);
    LCD_Message("wuwuwu");
    msDelay(1000);
    LCD_Clear();
  }
}

/* Misc routines */

void setupPorts (void)
{
  /* set Px0-Px5 as outputs */
  DDR = 0x3F;
}

void msDelay (int delay)
{
  for (int i = 0; i < delay; i++) {
    _delay_ms(1);
  }
}

/* LCD routines */

void pulseEnableLine (void)
{
  setBit(PORT, LCD_E);
  msDelay(5);
  clearBit(PORT, LCD_E);
}

void sendNibble (byte data)
{
  // clear data lines
  PORT &= 0xF0;
  if (data & _BV(3)) setBit(PORT, DAT7);
  if (data & _BV(2)) setBit(PORT, DAT6);
  if (data & _BV(1)) setBit(PORT, DAT5);
  if (data & _BV(0)) setBit(PORT, DAT4);
  //msDelay(30);
  pulseEnableLine();
}

void sendByte (byte data)
{
  sendNibble(data >> 4);
  sendNibble(data);
}

void LCD_Cmd (byte cmd)
{
  clearBit(PORT, LCD_RS);
  sendByte(cmd);
}

void LCD_Char (byte ch)
{
  setBit(PORT, LCD_RS);
  sendByte(ch);
}

void LCD_Init (void)
{
  
  LCD_Cmd(0x33); // initialize controller
  LCD_Cmd(0x32); // set to 4-bit input mode
  LCD_Cmd(0x28); // 2 line, 5x7 matrix
  LCD_Cmd(0x0C); // turn cursor off (0x0E to enable)
  LCD_Cmd(0x06); // cursor direction = right
  LCD_Cmd(0x01); // start with clear display
  msDelay(3);
}

void LCD_Clear (void)
{
  LCD_Cmd(CLEARDISPLAY);
  msDelay(3);
}

void LCD_Home (void)
{
  LCD_Cmd(SETCURSOR);
}

void LCD_Goto (byte x, byte y)
{
  byte addr = 0;
  switch (y) {
  case 1: addr = 0x40; break;
  case 2: addr = 0x14; break;
  case 3: addr = 0x54; break;
  }
  LCD_Cmd(SETCURSOR+addr+x);
}

void LCD_Line (byte row)
{
  LCD_Goto(0, row);
}

void LCD_Message (const char *text)
{
  while (*text) {
    LCD_Char(*text++);
  }
}
